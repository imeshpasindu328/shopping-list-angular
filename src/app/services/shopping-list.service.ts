import { Item } from '../interfaces/item';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  private listShopping: Item[];

  constructor() {
    this.listShopping = JSON.parse(localStorage.getItem('items') || '[]');
  }

  getListShopping() {
    return this.listShopping;
  }

  createItem(nameItem: string) {
    const id = this.listShopping.length + 1;
    const item: Item = {
      id: id,
      name: nameItem,
      date: new Date().toLocaleString('pt-BR'),
      purchased: false
    }

    return item;
  }

  addingItemList(nameItem: string) {
    const item = this.createItem(nameItem);
    this.listShopping.push(item);
    /* this.updatedLocalStorage(); */
  }

  updatedItemList(itemPrevious: Item, nameEditedItem: string) {
    const itemEdited: Item = {
      id: itemPrevious.id,
      name: nameEditedItem,
      date: itemPrevious.date,
      purchased: itemPrevious.purchased
    }

    const id = itemPrevious.id;
    this.listShopping.splice(Number(id) - 1, 1,
      itemEdited);

    /* this.updatedLocalStorage(); */
  }

  updatedLocalStorage() {
    localStorage.setItem(
      'items',
      JSON.stringify(this.listShopping)
    );
  }
}
