import { Item } from './../../interfaces/item';
import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit, OnChanges, OnDestroy {

  @Input() item!: Item;
  @Output() issueItemToEdit = new EventEmitter();
  @Output() issueItemToRemove = new EventEmitter();

  constructor() { }

  ngOnInit(): void {}

  ngOnChanges() {}

  updatedItem() {
    this.issueItemToEdit.emit(this.item);
  }

  removeItem() {
    this.issueItemToRemove.emit(this.item.id);
  }

  ngOnDestroy() {

  }
}
