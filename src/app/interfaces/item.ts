export interface Item {
  id?: number | string,
  name: string,
  date: Date | string,
  purchased: boolean
}
