import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Item } from 'src/app/interfaces/item';
import { ShoppingListService } from 'src/app/services/shopping-list.service';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.css']
})
export class InputComponent implements OnInit, OnChanges {
  @Input() itemThatWillBeEdited!: Item;
  editing = false;
  textBtn = 'Salvar item';

  valueItem!: string;

  constructor(
    private listService: ShoppingListService
  ) { }

  ngOnInit(): void {
  }

  addingItem() {
    this.listService.addingItemList(this.valueItem);
    this.clearField();
  }

  clearField() {
    this.valueItem = '';
  }

  updatedItem() {
    this.listService.updatedItemList(
      this.itemThatWillBeEdited,
      this.valueItem
    );

    this.clearField();
    this.editing = false;
    this.textBtn = 'Salvar item';
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (!changes['itemThatWillBeEdited'].firstChange) {
      this.editing = true;
      this.textBtn = 'Editar item';
      this.valueItem = this.itemThatWillBeEdited?.name;
    }
  }
}
