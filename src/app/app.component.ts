import { Component, DoCheck, OnInit } from '@angular/core';
import { Item } from './interfaces/item';
import { ShoppingListService } from './services/shopping-list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, DoCheck {
  title = 'angular-shopping-list';

  listShopping!: Array<Item>;
  itemToBeEdited!: Item;

  constructor(
    private listService: ShoppingListService
  ) { }

  ngOnInit(): void {
    this.listShopping = this.listService.
      getListShopping();

    console.log(this.listShopping);
  }

  updatedItem(item: Item) {
    this.itemToBeEdited = item;
  }

  removeItem(id: number) {
    const index = this.listShopping.findIndex((item) => item.id === id);

    this.listShopping.splice(index, 1);
  }

  clearList() {
    this.listShopping = [];
  }

  ngDoCheck(): void {
    this.listService.updatedLocalStorage();
  }
}
