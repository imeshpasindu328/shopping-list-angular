# Shopping list (Angular) 

## About

Task Manager with Angular is a web application that offers users the ability to manage their tasks efficiently. The application offers several features, including:

- Add Tasks.
- Edit Tasks.
- List Tasks.
- Remove Tasks.
- Temporarily Clear Screen.
- Prioritization of Tasks.

## Layout

**Link**: [Angular Shopping List](https://shopping-list-angular.vercel.app/)

![](src/assets/gifs/angular-shopping-list.gif)

## How tu run the project?

```bash
git clone https://gitlab.com/mateusdev340/shopping-list-angular.git # Gitlab
git clone https://github.com/mateusdev340/shopping-list-angular.git # Github

cd angular-shoppinh-list
npm install
ng serve
```

## Technologies

The following tools were used in the construction of the project:

### Frontend

- Angular
- TypeScript

## Contributors

<table>
    <thead>
        <tr>
            <td>
                <img src="https://avatars.githubusercontent.com/u/136182787?v=4" width="150px"/>
            </td>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>Mateus Maciel</th>
        </tr>
    </tbody>
</table>

## How to contribute?

1. Fork the project.
2. Create a new branch with your changes: `git checkout -b my-feature`
3. Save your changes and create a confirmation message telling you what you did: `git commit -m "modified file"`
4. Push your changes: `git push origin my-feature`

## License

This project is licensed under the Alura Cursos license.
